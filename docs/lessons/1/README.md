Conceitos básicos para o curso
===

# Revisão de conceitos

- **Cloud**: Reflexão - Como você define Cloud?
- **Managed Services (Serviços Gerenciados)**: são soluções onde o provedor de nuvem provê parte do gerenciamento de um determinado serviços de rede, como bancos de dados, pipelines, cache, fila, etc. de modo que seus clientes não precisem se importar (ou se importem muito pouco) com detalhes de baixo nível referentes a instalação e governança (patch management, monitoramento, backups e similares). Isso aproxima as equipes técnicas das regras de negócios agilizando a entrega de soluções. É importante lembrar que, isso não isenta a equipe totalmente de questões técnicas referente as tecnologias envolvidas, por exemplo, em um serviço gerenciado de um banco relacional, ainda é preciso se importar com a modelagem de dados.

## Abordagens de workloads na nuvem

A nuvem permite que criemos ambientes de diferentes formas. A seguir uma explanação das possibilidades.

- **IaaS (Infrastructure As A Service, ou Infraestrutura como Serviço)**: voltado a ambientes mais tradicionais, onde a nuvem é utilizada como um ambiente de hosting de instâncias virtuais como nenhum ou poucos serviços gerenciados. Neste tipo de workload encontramos soluções de runtime de aplicações ou serviços de rede (bancos de dados, cache, fila, etc) sendo executados dentro de instâncias virtuais convencionais. Exemplos AWS: EC2, GCP Compute Engine, Azure VM.
- **PaaS (Platform As A Service, ou Plataforma como Serviço)**: são ambientes que se baseiam em uso extensivo de serviços gerenciados para aplicações. Sistemas de PaaS geralmente proveêm mecanismos de deploy que podem ser facilmente integrados com pipelines de CI/CD, abstraem recursos como balanceamento e autoscaling e normalmente isentam equipes de governança de se preocuparem como detalhes de sistemas operacionais. Muitas vezes Managed Services individuais são considerados PaaS por si próprios. Exemplos AWS Elastic BeanStalk, GCP App Engine, Azure App Service.
- **Containers Orchestrators (Orquestradores de Containers)**: são um meio do caminho entre IaaS e PaaS e estão virando um padrão de mercado para entrega de soluções para executar aplicações, principalmente as baseadas em microserviços. Exemplos: Kubernetes, Swarm, AWS ECS, AWS EKS, GCP Container Engine, Azure AKS.
- **FaaS (Function As A Service, ou Funções como Serviço)**: ambientes destinados exclusivamente a aplicações usando funções. Em combinação com serviços gerenciados são categorizados como Serverless, pois excluem mais ainda a necessidade de se preocupar com sistemas operacionais ou containers. Exemplos: AWS lambda, Google Functions e Azure Functions.
- **SaaS (System As A Service, ou Sistemas como Serviços)**: englobam a entrega de uma solução de alto nível que abrange múltiplos workloads como ERP, monitoramento, ingestão e análise de logs, etc. Exemplos: SalesForce, New Relic, Loggly, DataDog. Normalmente este tipo de workload depende muito de automação pois é um produto final de venda e tem muitas partes móveis.

# Automação

*"Processo em que usamos tecnologias para agilizar entrega de tarefas"*

- Porque automatizar:
    - Agilizar tarefas repetidas: é possível executar repetidamente uma tarefa com um mínimo de esforço. Na nuvem é muito útil quando temos várias partes de um ambiente que se repetem como ambientes de homologação, staging produção ou stacks de aplicação que tem as mesmas partes (balanceador, cluster de aplicação, cache e banco de dados) ou ainda que o deploy de uma aplicação está com a versão correta.
    - Garantir compliance: toda tarefa executada por automação será idempotente, ou seja, vai ter o mesmo resultado dado um conjunto de parâmetros de entrada. Isso ajuda a garantir que ambientes de homologação, dev, staging e produção serão identicos em termos de arquitetura infraestrutural (mas não necessariamente em tamanho), ou que um sistema operacional sempre esta em conformidades de segurança ou operação, ou que um container terá todas as dependências instaladas.
- Quando não automatizar: quando nenhuma das exigências acima se aplicar. Automatizar por si é problemático pois o esforço de automação é maior do que a realização de tarefas de maneira manual, o que atrasa a entrega em vez de agilizar.

## Tipos de automação

- **Schedulled Jobs (Tarefas agendadas)**: execuções de ações repetidas em um intervalo de tempo como backups, expurgos, rotação de logs, etc. Abrange desde cron jobs de sistema operacionais até gerenciadores de tarefas. Exemplo: Linux crontab, Windows Scheduller, RunDeck, AWS CloudWatch Rule.
- **Infrastructure as Code (IaC, Infraestrutura como código)**: muito comum em soluções na nuvem uma vez que ela provê serviços por API. É capaz de construir desde simples recursos até arquiteturas inteiras de aplicações através de código declarativo. Exemplos, AWS Cloud Formation, GCP Cloud Deployment Manager, Azure Resource Manager, Terraform.
- **Configuration Managers (Gerenciadores de Configurações)**: É o que muitas pessoas ainda pensam quando se fala de automação. Gerenciadores de configuração foram criados para automatizar tarefas administrativas no nível de Sistemas Operacionais e serviços que rodam nele através de código declarativo (algumas vezes imperativo). Muito usado para criar usuários, instalar pacotes, configurar permissões, gerar arquivos de configuração ou executar deployment de aplicações. Alguns também abordam IaC na nuvem. Exemplos: Chef, CFEngine, Puppet, Ansible, Salt Stack.
- **Backed Images (Imagens pré definidas)**: Normalmente imagens de máquinas virtuais mas também incluso aqui imagens de containers. É quando deixamos imagens pré-prontas para subir determinados serviços sem precisar executar todos os passos já realizados dentro delas. Podem ser criadas manualmente, ou com ajuda de Configuration Managers e geradores de imagens. Existem diversos tipos de abordagens aqui, a AWS por exemplo classifica imagens AMI de EC2 conforme a seguir:
    - **Silver Images**: quando criamos imagens que possuem parte dos pré-requisitos de execução de um runtime, por exemplo, quando se é instalado JVM, Tomcat e outras dependências para uma aplicação web em Java mas não a aplicação em si. Quando uma instância é lançada a partir de uma imagem silver é esperado que outros processos (manuais ou automatizados) vão realizar os últimos passos como instalar o artefato da aplicação e/ou acertar detalhes de configuração (apontamento de banco de dados ou outros backends). Imagens Docker, geralmente são consideradas silver dado que a configuração é normalmente via variáveis de ambientes gerenciadas por um Container Orchestrator. A vantagem aqui é a personalização por ambiente e reuso de imagens em diferentes ambientes.
    - **Golden Images**: quando criamos imagens com todos os pré-requisitos de um runtime (aplicação e configuração inclusa) fazendo com que o artefato da aplicação seja a própria imagem. Isso tem o benefício de velocidade de bootstrap de uma nova instância pois ela já sobe com tudo pronto.

    É possível ter abordagens híbridas com imagens silver de sistema operacionais com certas partes básicas sempre pré-instaladas (como agentes de monitoramento, log shipping e atualizações de segurança) e gerar imagens gold a partir destas.

    Sistemas que escalam automaticamente (como AWS Autoscaling) normalmente utilizam um destes tipos de imagens dado que o tempo de executar bootstrap em todas estas operações podem gerar um tempo longo para disponibilizar uma nova instância.

- **Pipelines (linhas ou esteiras de produção)**: de sistemas de tratamento de dados para Analitics, Data Science ou Big Data até esteiras de entrega de aplicação de Continuous Delivery, passando por tarefas administrativas na cloud, pipelines permitem que uma sequência de tarefas sejam executadas serial ou paralelamente. Definir este tipo de automação pode ser simples (conjunto de jobs encadeados em soluções de CI/CD) ou bem complexas (ingestão e tratamento de dados para Big Data que dependem de múltiplas soluções que se acionam em sequência). Quando uma pipeline quebra e retorna um erro, normalmente todo processo é interrompido e é gerado alertas para as equipes responsáveis.

# Hands on acompanhado: Código e versionamento

Trabalhar com estes conceitos de automação dependem de código declarativo. Uma vez que aplicações também são criadas atravez de código e a melhor forma de lidar com código é por versionamento do mesmo. Por isso este curso se baseia absolutamente em gerenciar o código dos exercícios através de repositórios Git.

Assim como as aulas e exercícios propostos são fornecidos através deste repositório, as soluções dos exercícios devem ser realizadas por commit/push em repositórios individuais. Isso visa auxiliar o aluno a absorver os conceitos e criar uma rotina de uso de versionadores de código.

O professor vai apresentar o processo de criação de um Repositório em uma conta gratuíta no BitBucket, e também proverá acesso ao repositório privado do curso para que o aluno possa pegar snippets de código para auxiliar no Hands on.

# Hands on acompanhado: Familizarização com o Terraform

Com objetivo de introduzir o aluno ao conceito de IaC e à ferramenta, iremos realizar a criação de uma parte da Infraestrutura necessária para fornecimento de um bucket para site estáticos.
