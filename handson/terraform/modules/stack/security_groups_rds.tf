resource "aws_security_group" "rds_mariadb" {
  name        = "${local.rds_sg_name}"
  description = "Allow HTTP connections"
  vpc_id      = "${aws_vpc.this.id}"

  tags = "${merge(
    map("Name", "${local.rds_sg_name}"),
    local.common_tags
  )}"
}

resource "aws_security_group_rule" "rds_mariadb_ingress" {
  security_group_id = "${aws_security_group.rds_mariadb.id}"
  type              = "ingress"
  from_port         = 3306
  to_port           = 3306
  protocol          = "tcp"
  cidr_blocks       = ["${var.allow_from_ips}"]
  description       = "Allow HTTP from specific IPs"
}

resource "aws_security_group_rule" "rds_mariadb_from_ec2_ingress" {
  security_group_id        = "${aws_security_group.rds_mariadb.id}"
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.ec2_http.id}"
  description              = "Allow connections from instances"
}

resource "aws_security_group_rule" "rds_mariadb_from_ecs_ingress" {
  security_group_id        = "${aws_security_group.rds_mariadb.id}"
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.ecs_http.id}"
  description              = "Allow connections from instances"
}

resource "aws_security_group_rule" "rds_mariadb_egress" {
  security_group_id = "${aws_security_group.rds_mariadb.id}"
  type              = "egress"
  from_port         = -1
  to_port           = -1
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
}
