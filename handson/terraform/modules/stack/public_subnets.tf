resource "aws_internet_gateway" "this" {
  vpc_id = "${aws_vpc.this.id}"

  tags = "${merge(
    map("Name", "${local.common_name}"),
    local.common_tags
  )}"
}

resource "aws_route_table" "this_public" {
  vpc_id = "${aws_vpc.this.id}"

  tags = "${merge(
    map("Name", "${local.pub_subnets_name}"),
    local.common_tags
  )}"
}

resource "aws_route" "default_public_route" {
  route_table_id         = "${aws_route_table.this_public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.this.id}"
}

resource "aws_subnet" "this_public" {
  vpc_id                  = "${aws_vpc.this.id}"
  cidr_block              = "${element(var.public_cidr_blocks, count.index)}"
  availability_zone       = "${element(local.azs, count.index)}"
  map_public_ip_on_launch = true

  tags = "${merge(
    map(
      "Name",
      "${local.pub_subnets_name}-${local.azs[count.index]}"
    ),
    local.common_tags
  )}"

  count = 2
}

resource "aws_route_table_association" "this" {
  subnet_id      = "${element(aws_subnet.this_public.*.id, count.index)}"
  route_table_id = "${aws_route_table.this_public.id}"

  count = 2
}
