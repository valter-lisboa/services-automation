resource "aws_iam_user" "wordpress" {
  name = "${local.common_name}"
  path = "/"
}

resource "aws_iam_access_key" "wordpress" {
  user = "${aws_iam_user.wordpress.name}"
}

resource "aws_iam_user_policy_attachment" "attach_s3_permition_on_wordpress_user" {
  user       = "${aws_iam_user.wordpress.name}"
  policy_arn = "${aws_iam_policy.wordpress_s3.arn}"
}

output "wordpress_access_key" {
  value = "${aws_iam_access_key.wordpress.id}"
}

output "wordpress_secret_key" {
  value = "${aws_iam_access_key.wordpress.secret}"
}
