variable "name" {
  type        = "string"
  description = "A name to label resources"
}

variable "environment" {
  type        = "string"
  description = "The environment which the stack belongs"
}

variable "vpc_cidr_block" {
  type        = "string"
  description = "The VPC network CIDR Block"
}

variable "allow_from_ips" {
  type        = "list"
  description = "A list of IPs to allow connection"
}

variable "public_cidr_blocks" {
  type        = "list"
  description = "A list with 2 public subnets CIDR blocks"
}

variable "instance_type" {
  type        = "string"
  description = "The EC2 instance type"
  default     = "t2.micro"
}

variable "key_name" {
  type        = "string"
  description = "The EC2 key pair name"
}

variable "tags" {
  type        = "map"
  description = "A map with extra tags (up to 49) to be put in the resources. The Name and environment tags are already handled by the blueprint and should not be referenced here"
  default     = {}
}
