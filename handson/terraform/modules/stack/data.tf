locals {
  common_name         = "${var.name}-${var.environment}"
  pub_subnets_name    = "pub-${local.common_name}"
  ec2_role_name       = "ec2-${local.common_name}"
  ec2_ssh_sg_name     = "ec2-ssh-${local.common_name}"
  ec2_http_sg_name    = "ec2-http-${local.common_name}"
  elb_name            = "elb-${local.common_name}"
  elb_sg_name         = "${local.elb_name}"
  ecs_http_sg_name    = "ecs-http-${local.common_name}"
  alb_sg_name         = "alb-${local.common_name}"
  rds_sg_name         = "rds-${local.common_name}"
  statics_bucket_name = "assets-${local.common_name}"
  domain              = "${local.common_name}.cloud"

  azs = [
    "us-east-1a",
    "us-east-1b",
  ]

  common_tags = "${merge(
    map("Environment", "${var.environment}"),
    var.tags
  )}"
}
