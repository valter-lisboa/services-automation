resource "aws_security_group" "alb" {
  name        = "${local.alb_sg_name}"
  description = "Allow HTTP connections"
  vpc_id      = "${aws_vpc.this.id}"

  tags = "${merge(
    map("Name", "${local.alb_sg_name}"),
    local.common_tags
  )}"
}

resource "aws_security_group_rule" "alb_ingress" {
  security_group_id = "${aws_security_group.alb.id}"
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["${var.allow_from_ips}"]
  description       = "Allow HTTP from specific IPs"
}

resource "aws_security_group_rule" "alb_egress" {
  security_group_id = "${aws_security_group.alb.id}"
  type              = "egress"
  from_port         = -1
  to_port           = -1
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
}
