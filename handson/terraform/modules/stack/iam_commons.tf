data "aws_iam_policy_document" "wordpress_s3" {
  statement {
    sid       = "WordPressListS3"
    effect    = "Allow"
    resources = ["${aws_s3_bucket.static.arn}"]

    actions = [
      "s3:ListBucket",
      "s3:GetBucketLocation",
    ]
  }

  statement {
    sid       = "WordPressS3Access"
    effect    = "Allow"
    resources = ["${aws_s3_bucket.static.arn}/*"]

    actions = [
      "s3:ListObjects",
      "s3:GetObject*",
      "s3:PutObject*",
      "s3:DeleteObject*",
    ]
  }
}

resource "aws_iam_policy" "wordpress_s3" {
  name        = "s3-${local.common_name}"
  path        = "/"
  description = "Permissions for Wordpress to access the assets bucket"
  policy      = "${data.aws_iam_policy_document.wordpress_s3.json}"
}
