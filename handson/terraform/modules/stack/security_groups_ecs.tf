resource "aws_security_group" "ecs_http" {
  name        = "${local.ecs_http_sg_name}"
  description = "Allow HTTP connections"
  vpc_id      = "${aws_vpc.this.id}"

  tags = "${merge(
    map("Name", "${local.ecs_http_sg_name}"),
    local.common_tags
  )}"
}

resource "aws_security_group_rule" "ecs_http_ingress" {
  security_group_id = "${aws_security_group.ecs_http.id}"
  type              = "ingress"
  from_port         = 32000
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = ["${var.allow_from_ips}"]
  description       = "Allow HTTP from specific IPs"
}

resource "aws_security_group_rule" "ecs_http_from_alb_ingress" {
  security_group_id        = "${aws_security_group.ecs_http.id}"
  type                     = "ingress"
  from_port                = 32000
  to_port                  = 65535
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.alb.id}"
  description              = "Allow HTTP from load balancer"
}

resource "aws_security_group_rule" "ecs_http_egress" {
  security_group_id = "${aws_security_group.ecs_http.id}"
  type              = "egress"
  from_port         = -1
  to_port           = -1
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
}
