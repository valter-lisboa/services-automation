resource "aws_vpc" "this" {
  cidr_block                     = "${var.vpc_cidr_block}"
  instance_tenancy               = "default"
  enable_dns_support             = true
  enable_dns_hostnames           = true
  enable_classiclink             = false
  enable_classiclink_dns_support = false

  tags = "${merge(
    map("Name", "${local.common_name}"),
    local.common_tags
  )}"
}

resource "aws_vpc_dhcp_options" "this" {
  domain_name         = "${local.domain}"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags = "${merge(
    map("Name", "${local.common_name}"),
    local.common_tags
  )}"
}

resource "aws_vpc_dhcp_options_association" "this" {
  vpc_id          = "${aws_vpc.this.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.this.id}"
}

resource "aws_route53_zone" "this" {
  name          = "${local.domain}"
  comment       = "DNS zone for internal VPC elements in ${var.environment}"
  vpc_id        = "${aws_vpc.this.id}"
  force_destroy = true

  tags = "${merge(
    map("Name", "${local.domain}"),
    local.common_tags
  )}"
}
