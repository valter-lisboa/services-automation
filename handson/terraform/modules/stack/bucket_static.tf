resource "aws_s3_bucket" "static" {
  bucket        = "${local.statics_bucket_name}"
  acl           = "public-read"
  force_destroy = true

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = "${merge(
    map("Name", "${local.statics_bucket_name}"),
    local.common_tags
  )}"
}
