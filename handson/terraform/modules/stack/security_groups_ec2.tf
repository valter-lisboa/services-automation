resource "aws_security_group" "ec2_ssh" {
  name        = "${local.ec2_ssh_sg_name}"
  description = "Allow SSH connections"
  vpc_id      = "${aws_vpc.this.id}"

  tags = "${merge(
    map("Name", "${local.ec2_ssh_sg_name}"),
    local.common_tags
  )}"
}

resource "aws_security_group_rule" "ec2_ssh_ingress" {
  security_group_id = "${aws_security_group.ec2_ssh.id}"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["${var.allow_from_ips}"]
  description       = "Allow SSH from specific IPs"
}

resource "aws_security_group_rule" "ec2_ssh_egress" {
  security_group_id = "${aws_security_group.ec2_ssh.id}"
  type              = "egress"
  from_port         = -1
  to_port           = -1
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group" "ec2_http" {
  name        = "${local.ec2_http_sg_name}"
  description = "Allow HTTP connections"
  vpc_id      = "${aws_vpc.this.id}"

  tags = "${merge(
    map("Name", "${local.ec2_http_sg_name}"),
    local.common_tags
  )}"
}

resource "aws_security_group_rule" "ec2_http_ingress" {
  security_group_id = "${aws_security_group.ec2_http.id}"
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["${var.allow_from_ips}"]
  description       = "Allow HTTP from specific IPs"
}

resource "aws_security_group_rule" "ec2_http_from_elb_ingress" {
  security_group_id        = "${aws_security_group.ec2_http.id}"
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.elb.id}"
  description              = "Allow HTTP from load balancer"
}

resource "aws_security_group_rule" "ec2_http_egress" {
  security_group_id = "${aws_security_group.ec2_http.id}"
  type              = "egress"
  from_port         = -1
  to_port           = -1
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
}
