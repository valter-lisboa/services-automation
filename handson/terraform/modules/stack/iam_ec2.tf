data "aws_iam_policy_document" "ec2_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ec2" {
  name               = "${local.ec2_role_name}"
  path               = "/"
  assume_role_policy = "${data.aws_iam_policy_document.ec2_role.json}"
}

resource "aws_iam_instance_profile" "ec2" {
  name = "${local.ec2_role_name}"
  path = "/"
  role = "${aws_iam_role.ec2.name}"
}

resource "aws_iam_role_policy_attachment" "attach_s3_permition_on_wordpress_user" {
  role       = "${local.ec2_role_name}"
  policy_arn = "${aws_iam_policy.wordpress_s3.arn}"
}
